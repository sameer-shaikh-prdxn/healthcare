<div class="wrapper">

	<?php 
		$methods = get_field('methods');
		for( $i=0 ; $i<count($methods) ; $i++ ) { ?>
		<div class="sidebar-item">
			<a href="#"><h3><?php echo $methods[$i]['title'];?></h3></a>
			<p><?php echo $methods[$i]['desc'];?></p>
		</div>	
	<?php	}
	?>
</div>