<?php
get_header();

if ( have_posts() ) : 
	while ( have_posts() ) : the_post();
?>	
	
	<div class="wrapper">
		<img src="<?php   echo get_field('icon')['url'];?>">
		<h2><?php  the_title();?></h2>
		<p><?php  echo get_field('desc');?></p>
	</div>


<?php
get_footer();
?>