<?php
/* 	-------------------------------------------------------
	--------------------             Theme Customizer for header logo -------------------
	---------------------------------------	
*/ 
function my_theme_customizer_header_logo( $wp_customize ){
	$wp_customize->add_section( 'header_logo' , array(
	    'title'      => 'Select Header Image',
	    'priority'   => 30,
	));
	//select image
	 $wp_customize->add_setting('header_logo_s', array(
      'default'           => 'image.png',
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'header_logo_s', array(
      'label'    => 'Upload Logo Image',
      'section'  => 'header_logo',
      'settings' => 'header_logo_s',
      'width' => 50,
    	'height' => 50
    )));	
}
add_action('customize_register', 'my_theme_customizer_header_logo');
add_action( 'wp_head', 'cd_customizer_css_for_header_logo');
function cd_customizer_css_for_header_logo()
{
	$id= get_theme_mod('cat_setting_image');
   	?>
      <style type="text/css">
      /* .logo  { background:   url("<?php// echo get_theme_mod('header_logo_s'); ?>");} */
     </style>
   <?php
}


/* 	-------------------------------------------------------
	--------------------             Theme Customizer for Copyright Text  -------------------
	---------------------------------------	
*/ 
function my_theme_customizer_copyright_text( $wp_customize ){
	$wp_customize->add_section( 'copy_text' , array(
	    'title'      => 'Enter Company Name ',
	    'priority'   => 30,
	));
	//select image
	 $wp_customize->add_setting('copy_text_s', array(
      'default'           => 'NSD',
    ));
    $wp_customize->add_control('copy_text_se', array(
         'label'      => 'Company Name',
         'section'    => 'copy_text',
         'settings'   => 'copy_text_s',
     ));
}
add_action('customize_register', 'my_theme_customizer_copyright_text');
add_action( 'wp_head', 'cd_customizer_css_for_copyright_text');
function cd_customizer_css_for_copyright_text()
{
	$text = get_theme_mod('copy_text_se');
   	?>
      <style type="text/css">
      .copyright  { content:  "<?php echo get_theme_mod('copy_text_se','NSD');?> " ;}
     </style>
   <?php
}


/* 	-------------------------------------------------------
	--------------------             Register Navigation menu -------------------
	---------------------------------------	
*/ 

	register_nav_menus(array(
		'primary' => __('primary Menu') ,
		'footer'  => __('Footer Menu') ,
	));	

/* 	-------------------------------------------------------
	--------------------             Enquing Slick Slider -------------------
	---------------------------------------	
*/ 

	function enqueue_my_scripts(){
			wp_enqueue_style('my-style', get_stylesheet_uri());
			wp_enqueue_style('bx-style', 'https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css');
			wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), true);
			wp_enqueue_script('bx-jquery', 'https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js',array(), true);
			wp_enqueue_script('my-script-js',  get_template_directory_uri() . '/js/myscript.js' , array('jquery'), 1.0 ,true);

	}
	add_action('wp_enqueue_scripts', 'enqueue_my_scripts');


	/* 	-------------------------------------------------------
		--------------------          Custom Post for Services  -------------------
		---------------------------------------	
	*/ 


	function custom_post_service(){
		$labels  = array(
				'name' => 'Services',
				'singular_name' => 'Service',
				'add_new' => 'Add Item',
				'all_items' => 'All Items',
				'add_new_item' => 'Add Item',
				'edit_item' => 'Edit Item',
				'new_item' => 'New Item',
				'view_item' => 'View Item',
				'search_item' => 'Search Service',
				'not_found' => 'No item found',
				'not_found_in_trash' => 'No item found in trash',
				'parent_item_colon' => 'Parent Item'

		);
		$args = array(
				'labels' => $labels,
				'public' => true,
				'has_archive' => true,
				'publicly_queryable' => true,
				'query_var' => true,
				'rewrite' => true,
				'capability_type' => 'post',
				'hierarchical' => false,
				'supports' => array(
						'title',
						'editor',
						'excerpt',
						'thumblnail',
						'revision'
				),
				'taxonomies' => array('category', 'post_tag'),
				'menu_position' => 5,
				'exclude_from_search' => false
		);
		register_post_type('services', $args);
}
add_action('init', 'custom_post_service');
	
?>