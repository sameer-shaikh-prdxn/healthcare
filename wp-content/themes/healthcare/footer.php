		<footer>
			<h3>
				<img src="<?php echo get_field('logo')['url'];?>" alt="">
			</h3>
			<ul class="footer-ul">
				<?php
				$contact = get_field('contact'); ?>
				<li class="footer-li">
							<?php echo $contact['address'];?>
				</li>
				<li class="footer-li">
							<?php echo $contact['email'];?>
				</li>
				<li class="footer-li">
							<?php $footer_social = get_field('footer-social'); ?>
							<ul class="">
							<?php 	for($i=0; $i< count($footer_social); $i++){ ?>
								<li>
									<a href="<?php echo $footer_social[$i]['social_link'];?>"><img src="<?php echo $footer_social[$i]['social_img']['url']; ?>" alt=""></a></li>
							<?php } ?>
								
							</ul>
				</li>
			</ul>
			<nav class="footer-site-nav">
			<?php 
				$args =  array(
						   'theme_location' => 'footer'
				);
			?>
		<?php wp_nav_menu( $args ); ?>
		</nav>
		<span>&copy;2000-2018 BioWave Corporation 	</span>
		</footer>
		<?php wp_footer();?>
	</body>
</html>