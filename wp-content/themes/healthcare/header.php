<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php bloginfo('name');?></title>
  <?php wp_head();?>
</head>
<body <?php body_class();?>>
<header>
  <h1 class="logo-header">
    <a href="#"><img class="logo" src="<?php echo get_theme_mod('header_logo_s');?>" alt="logo"></a>
  </h1>
	<nav class="header-site-nav">
		<?php 
			$args =  array(
					   'theme_location' => 'primary'
					  );
		?>
    <?php wp_nav_menu( $args ); ?>
	</nav>
	<div class="header-bottom">
		<span>Home / Health care</span>
		<div class="header-bottom-social">
				<span>share this</span>
				<?php $header_social = get_field('header-social'); ?>
				<ul >
				<?php 	for($i=0; $i< count($header_social); $i++){ ?>
					<li>
						<a href="<?php echo $header_social[$i]['social-link'];?>">
							<img src="<?php echo $header_social[$i]['social-icon']['url']; ?>" alt=""></a></li>
				<?php } ?>
					
				</ul>
		</div>
	</div>


</header>

