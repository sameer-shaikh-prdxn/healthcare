<?php
get_header(); ?>
<main>
		<div class="wrapper">
	<!-- =================================================================== -->
	<!-- ======================       Healthcare Provider ========================-->
	<!-- ==================================================================== -->
	<div class="health-provider">
		<div class="wrapper">
			<h2><?php echo get_field('heading');?></h2>
			<p><?php echo get_field('desc');?></p>
			<a href="<?php echo get_field('link');?>">Schedule an service</a>
		</div>
	</div>
	<div class="divider"></div>


	<!-- =================================================================== -->
	<!-- ======================       aside ========================-->
	<!-- ==================================================================== -->

	
	<aside>
		<?php echo get_sidebar();?>
	</aside>


	<!-- ================================================================== -->
	<!-- ====================    SERIVICES  =================================-->
	<!-- =================================================================== -->
	<div class="services">
		<div class="wrapper">
			<?php 
			$the_query = new WP_Query(array('post_type'  => 'services'));
			if( $the_query->have_posts()){
				while ($the_query->have_posts()) : $the_query->the_post(); ?>
					<div class="left">
						<img src="<?php  echo get_field('icon')['url'];?>" alt="">
					</div>
					<div class="right">
						<h2><?php  the_title();?></h2>
						<p><?php  echo get_field('excerpt');?></p>
						<a href="<?php the_permalink();?>">learn more</a>
					</div>
			<?php endwhile;
					}else {
							echo "nothing to display here";
					}
					wp_reset_postdata();
?>


		</div>
	</div>


	<!-- ================================================================== -->
	<!-- ===================     TESTIMONIALS  =============================-->
	<!-- ======================================================================== -->

	<div class="testimonials">
		<?php $testi = get_field('testi');
			for($i=0; $i<count($testi) ; $i++){
		?>
			<div class="testimonials-item">
					<p> <?php echo $testi[$i]['message'];?></p>		
					<span class="testimonials-info"><?php echo $testi[$i]['author']?></span>
					<span class="testimonials-info"><?php echo $testi[$i]['position'] .","?></span>
					<span class="testimonials-info"><?php echo $testi[$i]['country']?></span>		
			</div>	
	<?php 
	}
	?>	
	</div>
	</div>
</main>

<?php
get_footer();
?>